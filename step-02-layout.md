# Layout
* didalam tag HTML body mesti ada tag div yang memiliki class [container](https://getbootstrap.com/docs/4.3/layout/overview/#containers)
```html
<div class="container">
</div>
```

* didalam container kita pakai [grid](https://getbootstrap.com/docs/4.3/layout/grid/#how-it-works) yaitu
```html
<div class="row">
    <div class="col"></div>
</div>
```

kita menargetkan ke tampilan HP. maka kita pakai `col-sm`. aturan pemakain row dan col adalah 1 row = 12 col, kalau kelebihan col maka ia akan nambah ke bawahnya.

* tag html yang lain adalah
[heading](https://getbootstrap.com/docs/4.3/content/typography/#display-headings)
```html
<h1>Header 1</h1>
<h2>Header 2</h2>
...
<h6>Header 6</h6>
```

* [link](https://www.w3schools.com/html/html_basic.asp)
```html
<a href="www.google.com">ke google</a>
<br/>
<button>ini tombol</button>
<iframe width="420" height="315" src="https://www.youtube.com/xxx"/>
<br/>
<hr/>
```

* [list](https://getbootstrap.com/docs/4.3/content/typography/#lists)
```html
    <ul>
        <li>ayam</li>
        <li>sayur</li>
        <li>ikan</li>
    </ul>
```

* [table](https://getbootstrap.com/docs/4.3/content/tables/)
```html
    <table class="table">
        <thead>
            <tr>
                <th>no</th>
                <th>Nama</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Adrian</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Budi</td>
            </tr>
        </tbody>
    </table>
```

* image
```html
<img src="./picture.png">
```
ada opsi attribute yaitu `height` dan `width` dimana satuan nilai adalah `px`

* [card](https://getbootstrap.com/docs/4.3/components/card/#example)
```html
<div class="card">
    <div class="card-body">
        <h5 class="card-title"></h5>
        <p class="card-text"></p>
    </div>
</div>
```

* [nav](https://getbootstrap.com/docs/4.3/components/navs/#base-nav)
```html
<ul class="nav">
    <li class="nav-item">
        <a class="nav-link" href="#">Active</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
    </li>
</ul>
```

* [navbar](https://getbootstrap.com/docs/4.3/components/navbar/#supported-content)
```html
<nav class="navbar navbar-expand-lg">
    <a class="navbar-brand" href="#">Logo</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbar1">
        <ul class="navbar-nav mr-auto">
        </ul>
    </div>
</nav>

* [color](https://getbootstrap.com/docs/4.3/utilities/colors/#color)
primary, success, danger, warning
```html
    <p class="text-primary">primary</p>
    <p class="text-success">success</p>
```

* [background-color](https://getbootstrap.com/docs/4.3/utilities/colors/#background-color)
primary, success, danger, warning
```html
    <div class="bg-primary">background primary</div>
```

* [text-alignment](https://getbootstrap.com/docs/4.3/utilities/text/#text-alignment)
left, center, right
```html
    <p class="text-center">ini tengah</p>   
```

[selanjutnya](./step-03.md)