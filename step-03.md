2. bagaimana supaya website kita dikunjungi orang dan orang tersebut tertarik dengan jasa kita, (tulis dikertas paling atas)
    * tentukan target user butuhkan dan permasalahannya?
        misal saya target orang HRD yang membutuhkan orang, yang mampu menyelesaikan masalah di bagian xxx. (yang ini sebagai panduan untuk siapa kita bikin website)
    * bagaimana cara kita membantu menyelesaikan permasalahan user tersebut?
        dengan cara menyatakan "saya bisa mengatasi / menyelesaikan masalah xxx yang dihadapi bpk" dan sebagai referensi saya juga menyertakan skill & pengalaman & pendidikan saya. (yang ini langsung bikin website)
    * setelah di atas, maka kita juga memberikan info kontak kita whatsap & facebook
3. murid disuruh cari website yang ingin dia buat.
4. murid juga disuruh melihat ke contoh twitter [bootstrap](https://getbootstrap.com/docs/4.3/examples/)
5. setelah dapat murid pasti punya ide pemikiran mau bikin website seperti apa, di gambar di kertas yang sama tadi
6. setelah jadi baru kita mulai koding

# 3. let's coding
1. jelaskan cara penggunakan twitter bootstrap
1. bikin folder project melalui file explorer
2. buka vs code dan tambahkan folder yang telah di buat
3. bikin file dengan nama "index.html"
4. buka file tsb dan dimulai dengan tulisan html ... maka akan muncul autocomplete ...pilih html5. Hapus yang tidak dipakai, dan jelaskan tentang title & body
5. buka [twitter bootstrap](https://getbootstrap.com)
6. copy di bagian "bootstrap CDN" css & js
7. untuk merubah warna yang diingini buka di [bootswatch](https://bootswatch.com/) disana ada theme CSS. download dan pindahkan kedalam folder project tsb. ubah link css di index.html mengarah ke file yang di download barusan.
8.


1. di dalam body html kita bikin tag dengan div yang memiliki class container
2. selanjutnya terserah.



6. langsung bikin pakai twitter bootstrap
6.0.1 icon bisa di fontawesome
    file cssnya bisa kita pakai CDN yaitu [https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/fontawesome.min.css](https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/fontawesome.min.css)
    add di dalam index.html. sama seperti css.
    untuk icon yang ingin dicari silahkan ke sini [https://fontawesome.com/icons?d=gallery&m=free](https://fontawesome.com/icons?d=gallery&m=free)

6.1 kalo udah jadi bisa coba ke [google fonts](https://fonts.google.com/)

7. kalo udah jadi langsung ke netlify
8. add google CEO